/* eslint-disable dot-notation */
// --------------------------------------------------------------------------------
// Load environment variable from .env file if not supplied
const path = require('path');
require('dotenv').config({
  path: (typeof process.env.DOTENV_PATH !== 'undefined')
    ? path.resolve(process.cwd(), process.env.DOTENV_PATH)
    : path.resolve(process.cwd(), '.env'),
});
// --------------------------------------------------------------------------------
// Require tools
const _ = require('lodash');
const fs = require('fs');
const ARGS = require('shell-arguments');
const figlet = require('figlet'); // Just for fun, ASCII art FTW! ;)
/* const colors = */ require('colors');

const csvtojsonModule = require('csvtojson');

// Load package.json data (SECURITY WARNING, never use browserify with this)
const pckg = require('./package.json');

// Loading logger
const logger = require('./packets/logger.js');
// --------------------------------------------------------------------------------
// Asii art
console.log(figlet.textSync('UltiCAD', { font: 'Small' }).yellow);
console.log(figlet.textSync('CSV to HHA', { font: 'Small' }).yellow);
console.log();
console.log(' ############################################################################## '.red);
console.log('  Ovaj program zasticen je autorskim pravom autora, za dobivanje licence '.yellow);
console.log(`  kontaktirajte me na email: ${pckg.author.email} `.yellow);
console.log(`  Verzija programa ${pckg.version} | Licenca važi za: KLEK d.o.o.`.yellow);
console.log('  by Gordan Nekić '.yellow);
console.log();
console.log(' ############################################################################## '.red);
// --------------------------------------------------------------------------------
// Converter Class
const { Converter } = csvtojsonModule;
// --------------------------------------------------------------------------------

const ClearCroLetters = (str) => {
  return str
    .replace('č', 'c')
    .replace('ć', 'c')
    .replace('ž', 'z')
    .replace('š', 's')
    .replace('đ', 'd')
    .replace('Č', 'C')
    .replace('Ć', 'C')
    .replace('Ž', 'Z')
    .replace('Š', 'S')
    .replace('Đ', 'D');
};

// Generate HHA document
const GenerateHHA = (data, hhaOutputIn) => {
  let exportData = '';

  exportData += '"HHOS VS1.0.5.2" 6'; // Add prefix for data versioning
  exportData += '\n';

  let obj = {};
  let zaokret = '0';
  let valueKantDolje = '';
  let valueKantGore = '';
  let valueKantLijevo = '';
  let valueKantDesno = '';
  for (let i = 1; i <= data.length; i++) {
    obj = data[i - 1];

    logger.log('info', `Učitavam ${i}# element...`);
    logger.data(`Materijal: "${obj['Materijal']}-${obj['Dekor']}-${obj['Debljina']}" | Komada: ${obj['Kom']} | Duljina: ${obj['Duljina']}.0 | Širina: ${obj['Sirina']}.0 `);
    // exportData += 'E1 1796 "1" "" "" "Demo" 1 500.0 400.0 -1 500.0 400.0 0.0 0.0 0.0 0.0 5 5 5 5 -1 0 "" 1 0 0 I"" "" "" "" "" "" "" "" "" "" "" "Ormar" v"" "" 1 0.0 0.0 0.0 h"" "" 1 0.0 0.0 0.0 l"" "" 1 0.0 0.0 0.0 r"" "" 1 0.0 0.0 0.0 W0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0 0 0 0';
    // exportData += 'E'+i+' 1796 "'+i+'" "" "" "'+obj['Materijal']+'-'+obj['Dekor']+'-'+obj['Debljina']+'" '+obj['Kom']+' '+obj['Duljina']+'.0 '+obj['Sirina']+'.0 -1 '+obj['Duljina']+'.0 '+obj['Sirina']+'.0 0.0 0.0 0.0 0.0 5 5 5 5 -1 0 "" '+obj['Kom']+' 0 0 I"" "" "" "" "" "" "" "" "" "" "" "Element" v"" "" 1 0.0 0.0 0.0 h"" "" 1 0.0 0.0 0.0 l"" "" 1 0.0 0.0 0.0 r"" "" 1 0.0 0.0 0.0 W0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0 0 0 0';

    valueKantDolje = `${obj['KantDolje_Sifra']} ${obj['KantDolje_Vrsta']}`;
    valueKantGore = `${obj['KantGore_Sifra']} ${obj['KantGore_Vrsta']}`;
    valueKantLijevo = `${obj['KantLijevo_Sifra']} ${obj['KantLijevo_Vrsta']}`;
    valueKantDesno = `${obj['KantDesno_Sifra']} ${obj['KantDesno_Vrsta']}`;

    // NOT STANDARD ROTATION, REQUEST FROM WORKERS because they want
    if (parseInt(obj['Duljina'], 10) > parseInt(obj['Sirina'], 10)) {
      const temp = valueKantGore;
      valueKantGore = valueKantDesno;
      valueKantDesno = valueKantDolje;
      valueKantDolje = valueKantLijevo;
      valueKantLijevo = temp;
    }
    // WRONG !!! DANGER, NOT STANDARD ROTATION OF KANTS

    if (obj['Godovi'].toString() === '0') { zaokret = '-1'; /* Zaokret ENABLE! */ } else { zaokret = '0'; /* Zaokret DISABLE! */ }
    const programOutputString = _.get(obj, 'Program', '');
    const programBrojIFunkcija = `${ClearCroLetters(obj['Funkcija'])} - ${programOutputString}`;
    const oznakaElementa = _.get(obj, 'OznakaElementa', '');
    exportData += `E${i} 4 "${i}" "" "" "${ClearCroLetters(`${obj['Materijal']}-${obj['Dekor']}-${obj['Debljina']}`)}" ${obj['Kom']} ${obj['Duljina']},0 ${obj['Sirina']},0 ${zaokret} ${obj['Duljina']},0 ${obj['Sirina']},0 0,0 0,0 0,0 0,0 5 5 5 5 -1 0 "" ${obj['Kom']} 0 0 I"" "" "" "" "" "" "" "" "${obj['Barcode']}" "${oznakaElementa}" "${programBrojIFunkcija}" "${ClearCroLetters(obj['Kupac'])}" `; // Space on end is VERY IMPORTANT
    exportData += `v"${valueKantDolje}" "" 1 0,0 0,0 0,0 `;
    exportData += `h"${valueKantGore}" "" 1 0,0 0,0 0,0 `;
    exportData += `l"${valueKantLijevo}" "" 1 0,0 0,0 0,0 `;
    exportData += `r"${valueKantDesno}" "" 1 0,0 0,0 0,0 `;
    exportData += 'W0,0 0,0 0,0 0,0 0,0 0,0 0,0 0,0 0 0 0 0';
    exportData += '\n';
  }

  logger.info('Finaliziram podatke u HHA datoteku.');

  let hhaOutput = hhaOutputIn;
  if (typeof ARGS['o'] !== 'undefined') {
    hhaOutput = ARGS['o'].toString();
  }
  hhaOutput = ClearCroLetters(hhaOutput);
  fs.writeFile(hhaOutput, exportData, (error) => {
    if (error) { console.log(error); return; }
    logger.info('Datoteka je spremljena!');
  });

  return exportData;
};

// ######################################
// Start converting!

let csvFilename = './input.csv';
if (typeof ARGS['f'] !== 'undefined') {
  csvFilename = ARGS['f'].toString();

  const converter = new Converter({
    delimiter: ';',
  });

  converter.fromFile(csvFilename)
    .then((jsonObj) => {
      GenerateHHA(jsonObj, './output.HHA');
    })
    .catch((error) => {
      logger.error(error);
    });
} else {
  let totalData = [];
  let totalFiles = 0;
  let totalConvertedFiles = 0;

  const p = './ULAZ/';
  fs.readdir(p, (err, files) => {
    if (err) { logger.error(err); return; }

    files.map((file) => {
      return path.join(p, file);
    }).filter((file) => {
      return fs.statSync(file).isFile();
    }).forEach((file) => {
      if (path.extname(file).toLowerCase() === '.csv') {
        console.log('%s (%s) (%s)', file, path.extname(file), path.basename(file, path.extname(file)));
        totalFiles++;

        const converter = new Converter({
          delimiter: ';',
        });

        converter.fromFile(file)
          .then((jsonObj) => {
            const baseName = path.basename(file, path.extname(file));
            for (let i = 0; i < jsonObj.length; i++) {
              // eslint-disable-next-line no-param-reassign
              jsonObj[i]['Kupac'] = baseName;
            }

            totalData = totalData.concat(jsonObj);
            GenerateHHA(jsonObj, `${path.dirname(file)}/${baseName}.HHA`);

            totalConvertedFiles++;
            if (totalConvertedFiles === totalFiles) {
              logger.info('##### Converting total csv #####');
              GenerateHHA(totalData, `${path.dirname(file)}/TOTAL.HHA`);
            }
          })
          .catch((error) => {
            logger.error(error);
          });
      }
    });
  });
}

/*
  DEMO DATA from files...
*/

/*
"HHOS 2.0.0.4 " 7
E1 1796 "1" "" "" "Demo" 1 500.0 400.0 -1 500.0 400.0 0.0 0.0 0.0 0.0 5 5 5 5 -1 0 "" 1 0 0 I"" "" "" "" "" "" "" "" "" "" "" "Ormar" v"" "" 1 0.0 0.0 0.0 h"" "" 1 0.0 0.0 0.0 l"" "" 1 0.0 0.0 0.0 r"" "" 1 0.0 0.0 0.0 W0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0 0 0 0
E3 1796 "2" "" "" "Demo" 1 600.0 400.0 -1 600.0 400.0 0.0 0.0 0.0 0.0 5 5 5 5 -1 0 "" 1 0 0 I"" "" "" "" "" "" "" "" "" "" "" "Ormar" v"" "" 1 0.0 0.0 0.0 h"" "" 1 0.0 0.0 0.0 l"" "" 1 0.0 0.0 0.0 r"" "" 1 0.0 0.0 0.0 W0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0 0 0 0
E4 1796 "3" "" "" "Demo" 2 500.0 300.0 -1 500.0 300.0 0.0 0.0 0.0 0.0 5 5 5 5 -1 0 "" 2 0 0 I"" "" "" "" "" "" "" "" "" "" "" "Ormar" v"" "" 1 0.0 0.0 0.0 h"" "" 1 0.0 0.0 0.0 l"" "" 1 0.0 0.0 0.0 r"" "" 1 0.0 0.0 0.0 W0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0 0 0 0
E5 1796 "5" "" "" "Demo" 1 330.0 310.0 -1 330.0 310.0 0.0 0.0 0.0 0.0 5 5 5 5 -1 0 "" 1 0 0 I"" "" "" "" "" "" "" "" "" "" "" "Ormar" v"" "" 1 0.0 0.0 0.0 h"" "" 1 0.0 0.0 0.0 l"" "" 1 0.0 0.0 0.0 r"" "" 1 0.0 0.0 0.0 W0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0 0 0 0
BD1 2800.0 2070.0 2 "Demo" 0 0 0 0 0 "" "" 0 0 0
*/

/*
"HHOS VS1.0.5.2" 6
OA4,4 3900,0 3100,0 1 0 0 0 0 0 15,0 0 0 ""
OE-1 -1 -1
OM-1 -1 0 -1 -1 -1 -1
E1 4 "1" "" "" "---" 1 550,0 400,0 0 557,0 400,0 0,0 0,0 3,0 4,0 0 0 0 0 -1 0 "" I"" "" "" "" "" "" "" "" "" "" "" "blabla" v"T ABS" "" 0 0,0 0,0 70,0 h"T Mel" "" 1 0,0 0,0 70,0 l"" "" 1 0,0 0,0 70,0 r"" "" 1 0,0 0,0 70,0 W0,0 0,0 0,0 0,0 0,0 0,0 0,0 0,0 0 0 0 0
E2 4 "2" "" "" "---" 1 400,0 550,0 0 400,0 550,0 0,0 0,0 0,0 0,0 5 5 5 5 -1 0 "" I"" "" "" "" "" "" "" "" "" "" "" "blabla" v"" "" 1 0,0 0,0 70,0 h"" "" 1 0,0 0,0 70,0 l"" "" 1 0,0 0,0 70,0 r"" "" 1 0,0 0,0 70,0 W0,0 0,0 0,0 0,0 0,0 0,0 0,0 0,0 0 0 0 0
E3 4 "3" "" "" "---" 5 100,0 120,0 -1 100,0 120,0 0,0 0,0 0,0 0,0 5 5 5 5 -1 0 "" I"" "" "" "" "" "" "" "" "" "" "" "blabla" v"" "" 1 0,0 0,0 70,0 h"" "" 1 0,0 0,0 70,0 l"" "" 1 0,0 0,0 70,0 r"" "" 1 0,0 0,0 70,0 W0,0 0,0 0,0 0,0 0,0 0,0 0,0 0,0 0 0 0 0
M"---" 0 -1 0
SOO1000,0 400,0 0,000 0,0 0,0 0,0 0,0 1 2 4 1 0 0 0 -1 0 0 19,0 0 0,00 30,0 650
SOE0 15,0 15,0 15,0 15,0 "" "" "" "" ""
SOM-1 -1 -1 0 0 20,0 0 4200 6500 0 0 0 0 0 0,0 0,0 0 0,0 0,0 0
SM0 "" 2800,0 2070,0 9999 0 100 15,0 15,0 15,0 15,0 -1 0 0 0
*/

/*
ID
???
Number
???
???
Material
Amount
Dimensions Lenght
Dimensions Width

Optimization Lenght
Optimization Width
*/
