const winston = require('winston'); // For logging!
const { format } = require('logform');

const myCustomLevels = {
  levels: {
    debug: 0,
    info: 1,
    data: 1,
    error: 2,
  },
  colors: {
    debug: 'magenta',
    info: 'yellow',
    data: 'blue',
    error: 'red',
  },
};

const myFormat = format.printf((info) => {
  return `${info.message}`;
});

const logger = winston.createLogger({
  levels: myCustomLevels.levels,
  // colors: myCustomLevels.colors,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        myFormat,
      ),
      level: 'info',
    }),
    new winston.transports.File({
      name: 'error',
      filename: './logs/converter.log',
      level: 'error',
    }),
  ],
});

module.exports = logger;
