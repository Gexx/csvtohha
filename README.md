# UltiCAD CSV to HHA converter

### Description
This is program that converts UltiCAD CSV file to HHA file, coded by Gordan Nekić.

### Installation
You need to run:
```sh
$ npm install
```

Ok, now you are ready to go!
```sh
$ node app.js --f "input_file" --o "output_file"
```

### Todos

 - Add more Code Comments
 - Fix bug where unexpectedly sometimes some function return "Cthulhu" ;)
